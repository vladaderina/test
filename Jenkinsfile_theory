// Документация: https://www.jenkins.io/doc/book/pipeline/syntax/

pipeline {
    agent (any, none) // выбирает на каком slave stage будет выполняться глобально 
    // agent any - пайплайн может быть выполнен на любом доступном агенте, 
    //agent none - пайплайн должен быть выполнен на мастере без использования агента.

    options {
        // Timeout counter starts AFTER agent is allocated
        timeout(time: 1, unit: 'SECONDS')
        // Specifying a global execution timeout of one second,
        // after which Jenkins will abort the Pipeline run.
    }
    stages {
        stage('Example') {
            agent { 
                label 'my-defined-label' // запуск агента slave по метке
            }
            options {
                // Timeout counter starts BEFORE agent is allocated
                timeout(time: 1, unit: 'SECONDS')
            }
            steps {
                echo 'Hello World'
            }
        }
    }
}

///////////////////////////////////////////////////////////////////////////

pipeline {
    agent {
        //ИЛИ вариант №1
        docker { // запускает образ внутри slave и внутри выполняет stages
            image 'myregistry.com/node'
            label 'my-defined-label'
            registryUrl 'https://myregistry.com/'
            registryCredentialsId 'myPredefinedCredentialsInJenkins'
        }
        //ИЛИ вариант №2
        // Equivalent to "docker build -f Dockerfile.build --build-arg version=1.0.2 ./build/
        dockerfile {
        filename 'Dockerfile.build'
        dir 'build'
        label 'my-defined-label'
        additionalBuildArgs  '--build-arg version=1.0.2'
        args '-v /tmp:/tmp' // аргументы при docker run
        }
    }

    stages {
        stage('Example') {
            steps {
                echo 'Hello World'
            }
        }
    }
}

/////////////////////////////////////////////////////////
pipeline {
    agent any  
    parameters { // задаются перед запуском pipeline и доступны по всему файлу
        string(name: 'PARAMETER_NAME', defaultValue: 'default_value', description: 'Description of the parameter')
    }
    stages {
        stage('Example Username/Password') {
            environment { // задаются как переменные окружения 
                SERVICE_CREDS = credentials('my-predefined-username-password') // доступ к данным по id credentials
            }
            steps {
                sh 'echo "Service user is $SERVICE_CREDS_USR"'
                sh 'echo "Service password is $SERVICE_CREDS_PSW"'
                sh 'curl -u $SERVICE_CREDS https://myservice.example.com'
                echo "Hello ${params.PARAMETER_NAME}"
            }
        }
        stage('Example SSH Username with private key') {
            environment {
                SSH_CREDS = credentials('my-predefined-ssh-creds')
            }
            steps {
                sh 'echo "SSH private key is located at $SSH_CREDS"'
                sh 'echo "SSH user is $SSH_CREDS_USR"'
                sh 'echo "SSH passphrase is $SSH_CREDS_PSW"'
            }
        }
    }
}

///////////////////////////////////////////////////////////////////////////
pipeline {
    agent any

    options {
        timeout(time: 1, unit: 'HOURS') 
        newContainerPerStage() //Used with docker or dockerfile top-level agent. 
        //When specified, each stage will run in a new container instance on the same node, 
        //rather than all stages running in the same container instance.
    }
    stages {
        stage('Example') {
            steps {
                options {
                    timestamps() // время, в которое была отправлена строка
                    retry(3) // в случае сбоя повторить stage 3 раза
                }
                echo 'Hello World'
            }
        }
    }
}