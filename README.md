# Контейнер с настроенным proxy nginx для веб-сервера

## Описание файлов

- Dockerfile: файл для сборки образа веб-сервиса
- main.py: сервис на python с использованием библиотеки fastAPI
- test.conf: настройки proxy-сервера
- docker-compose.yml: настройки запуска контейнеров nginx и веб-сервиса

## Содержание Dockerfile
```dockerfile
FROM python:latest
COPY . /app
WORKDIR /app
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
ENTRYPOINT ["python", "main.py"]
```