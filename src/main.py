from fastapi import FastAPI
from fastapi.responses import FileResponse
from fastapi.responses import HTMLResponse
import os
import uvicorn
 
app = FastAPI()

@app.get("/", response_class = HTMLResponse)
def response():
    return "<h2>Index.html</h2>"

if __name__ == "__main__":
	ip = os.getenv("SERVER_IP", '0.0.0.0')
	port = int(os.getenv("SERVER_PORT", 5000))
	uvicorn.run(app, host=ip, port=port)
