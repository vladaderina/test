pipeline {
    agent any

    options {
        skipStagesAfterUnstable()
    }

    parameters {
        string(name: 'SERVER_IP', defaultValue: '0.0.0.0', description: '')
        string(name: 'SERVER_PORT', defaultValue: '5000', description: '')
    }

    environment {
        NAME_SERVER = 'web_server'
        NAME_NGINX = 'test_nginx'
        IMAGE_TAG = "v$BUILD_NUMBER"
        NETWORK = 'my_network'
        IMAGE_NAME_SERVER = "${env.NAME_SERVER}:${env.IMAGE_TAG}"
        IMAGE_NAME_NGINX = "${env.NAME_NGINX}:${env.IMAGE_TAG}"
        DOCKERFILE_NAME_SERVER = "Dockerfile_server"
        DOCKERFILE_NAME_NGINX = "Dockerfile_nginx"
    }

    stages {
        stage('Build') {
            stages {
                stage('Build server') {
                    steps {
                        script {
                            def dockerImageServer = docker.build("${env.IMAGE_NAME_SERVER}", "-f ./src/${env.DOCKERFILE_NAME_SERVER} ./src")
                            echo "Create Docker Image: ${env.IMAGE_NAME_SERVER}"
                        }
                    }
                }
                stage('Build nginx') {
                    steps {
                        script {
                            sh "envsubst < template.conf > test.conf"
                            def dockerImageNginx = docker.build("${env.IMAGE_NAME_NGINX}", "-f ${env.DOCKERFILE_NAME_NGINX} .")
                            echo "Create Docker Image: ${env.IMAGE_NAME_NGINX}"
                        }
                    }
                }
            }
        }

        stage('Integration tests') {
            steps {
                script {
                    sh "docker network create -d bridge $env.NETWORK"

                    def imageNginx = docker.image("${env.IMAGE_NAME_NGINX}")
                    def imageServer = docker.image("${env.IMAGE_NAME_SERVER}")

                    containerIdServer = imageServer.run("--name $env.NAME_SERVER --network $env.NETWORK -p ${SERVER_PORT}:${SERVER_PORT}")
                    containerIdNginx = imageNginx.run("--name $env.NAME_NGINX --network $env.NETWORK -p 80:80")

                    def ip_server = sh(script: "docker container inspect -f '{{ .NetworkSettings.Networks.${env.NETWORK}.IPAddress }}' ${containerIdServer.id}", returnStdout: true).trim()
                    def ip_nginx = sh(script: "docker container inspect -f '{{ .NetworkSettings.Networks.${env.NETWORK}.IPAddress }}' ${containerIdNginx.id}", returnStdout: true).trim()

                    echo "IP nginx_test = $ip_nginx"
                    echo "IP web_server = $ip_server"

                    withEnv(["IPNGINX=$ip_nginx", "IPSERVER=$ip_server"]) {
                        sh "pip install -U pytest"
                        sh "pip install --upgrade pip"
                        sh "pip install -r ./src/requirements.txt"
                        sh "pytest ./tests/integration_tests/test_main.py"
                    }
                }
            }
            post {
                always {
                    script {
                        containerIdServer.stop()
                        containerIdNginx.stop()
                        sh "docker network rm $env.NETWORK"
                    }
                }
            }
        }
    }
}
