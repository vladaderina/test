import pytest
from bs4 import BeautifulSoup
from main import app
import requests
import os

ip_server = os.environ['IPSERVER']
ip_nginx = os.environ['IPNGINX']

class TestPort:
    @pytest.mark.parametrize("ip, port", [
        (ip_server, 5000), 
        (ip_nginx, 80)
    ])
    def test_port_good(self, ip, port):
        response = requests.get(f"http://{ip}:{port}/")
        assert response.status_code == 200, f"Res '{response.status_code}'"
        soup = BeautifulSoup(response.text, 'html.parser')
        expected_text = "Index.html"
        assert expected_text in soup.get_text(), f"Expected text '{expected_text}' not found in the HTML content"

    @pytest.mark.parametrize("ip, port, expectation", [
        (ip_server, 8080, requests.exceptions.ConnectionError)
    ])
    def test_port_bad(self, ip, port, expectation):
        with pytest.raises(expectation):
            response = requests.get(f"http://{ip}:{port}/")

@pytest.mark.parametrize("ip, port", [
    (ip_server, 5000), 
    (ip_nginx, 80)
])
class TestPath:
    @pytest.mark.parametrize("path", ["/", ""])
    def test_path_good(self, ip, port, path):
        response = requests.get(f"http://{ip}:{port}{path}")
        assert response.status_code == 200
    @pytest.mark.parametrize("path", ["/test", "/other"])
    def test_path_bad(self, ip, port, path):
        response = requests.get(f"http://{ip}:{port}{path}")
        assert response.status_code == 404

@pytest.mark.parametrize("ip, port", [
    (ip_server, 5000), 
    (ip_nginx, 80)
])
class TestMethod:
    def test_method_good(self, ip, port):
        response = requests.get(f"http://{ip}:{port}")
        assert response.status_code == 200
    @pytest.mark.parametrize("method", ["post", "delete", "put"]) 
    def test_method_bad(self, ip, port, method):
        if (method == "post"):
            response = requests.post(f"http://{ip}:{port}")
        elif (method == "delete"):
            response = requests.delete(f"http://{ip}:{port}")
        elif (method == "put"):
            response = requests.put(f"http://{ip}:{port}")
        assert response.status_code == 405

@pytest.mark.parametrize("ip, port", [
    (ip_server, 5000), 
    (ip_nginx, 80)
])
class TestHeaders:
    def test_header_good(self, ip, port):
        response = requests.get(f"http://{ip}:{port}")
        assert response.status_code == 200
        assert response.headers["Content-Type"] == "text/html; charset=utf-8"